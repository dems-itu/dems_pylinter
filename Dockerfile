FROM python:3.9.1-alpine AS base
WORKDIR /app

# Prepare dependencies
FROM base AS builder
COPY requirements.txt ./
RUN apk add --no-cache libgcc git build-base && pip install -r requirements.txt -t /app && \
    rm requirements.txt && rm -r /app/bin

# Copy dependency files
FROM base AS production
WORKDIR /app
COPY --from=builder /app /app
COPY execute.py /app
