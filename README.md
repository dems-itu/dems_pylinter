dems_pylinter ✂️🐍
================

  [![pipeline status](https://gitlab.com/dems-itu/dems_pylinter/badges/master/pipeline.svg)](https://gitlab.com/dems-itu/dems_pylinter/-/pipelines)

Python linter using flake8 for the Digital Exam Management System.

# Usage

```bash
# build the image
$ docker build -t dems/pylinter .
# make sure the image "dems/pylinter" is in the images list
$ docker images
# run the tests. Use "pwd" command to find the full path
$ docker run -v /full_path_to/tests:/app/files:ro dems/pylinter /app/execute.py
```

## COPYRIGHT

MIT License. Copyright (c) 2021 Emin Mastizada.

Check the LICENSE file provided with the project for the details.
