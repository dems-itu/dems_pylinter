#!/usr/bin/env python3
"""
DEMS Python Linter.

Check and return number of Python Code Syntax warnings using flake8.
Return syntax: number of issue;list of errors/warnings joined with dot.
Example: 2;E302 expected 2 blank lines, found 1. F841 local variable 'number' is assigned to but never used
"""
from contextlib import redirect_stdout
from flake8.api import legacy as flake8


def code_errors(filename):
    style = flake8.get_style_guide(ignore="E302")
    with open("/dev/null", "w") as output:
        with redirect_stdout(output):
            report = style.check_files([filename])
    return report.get_statistics("")


if __name__ == "__main__":
    errors = code_errors("/app/files/user_code.py")
    total_errors = len(errors)
    error_str = ". ".join([error.lstrip("1 ") for error in errors])
    print(f"{total_errors};{error_str}")
